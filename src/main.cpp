#include <cstdint>
#include <cstring>
#include <fstream>
#include <iostream>
#include <sys/mman.h>
#include <unistd.h>
#include <wayland-client-protocol.h>
#include <wayland-client.h>

#include "shm_util.h"
#include "xdg-shell-client-protocol.h"

#define u32 uint32_t
#define i32 int32_t
#define u8 uint8_t

using namespace std;

const int width = 256, height = 256;
const int stride = width * 4;
const int shm_pool_size = height * stride * 2;

class Client {
public:
  Client() {
    display = wl_display_connect(NULL);
    registry = wl_display_get_registry(display);
    wl_registry_add_listener(registry, &registry_listener, this);
    wl_display_roundtrip(display);

    check_values();

    surface = wl_compositor_create_surface(compositor);
    wm_surface = xdg_wm_base_get_xdg_surface(wm_base, surface);

    xdg_surface_add_listener(wm_surface, &surface_listener, this);
    toplevel = xdg_surface_get_toplevel(wm_surface);
    xdg_toplevel_set_title(toplevel, "Hello World");
    wl_surface_commit(surface);

    while (wl_display_dispatch(display)) {
    }
  }

  ~Client() {
    wl_display_disconnect(display);
    munmap(pool_data, shm_pool_size);
    close(fd);
  }

private:
  /* GLOBALS */
  wl_display *display;
  wl_registry *registry;
  wl_shm *shm; // SHARED MEMORY
  wl_compositor *compositor;
  wl_output *output;
  xdg_wm_base *wm_base;

  wl_surface *surface;
  xdg_surface *wm_surface;
  xdg_toplevel *toplevel;

  int fd;        // file descriptor
  u8 *pool_data; // shm pool data
  wl_shm_pool *pool;

  static const wl_registry_listener registry_listener;
  static const xdg_wm_base_listener wm_base_listener;
  static const wl_buffer_listener buffer_listener;
  static const xdg_surface_listener surface_listener;

  void check_values() {
    if (display == nullptr) {
      cerr << "Display not found" << endl;
      exit(1);
    } else {
      cout << "Display found" << endl;
    }

    if (compositor == nullptr) {
      cerr << "Compositor not found" << endl;
      exit(1);
    } else {
      cout << "Compositor found" << endl;
    }

    if (shm == nullptr) {
      cerr << "Shared memory not found" << endl;
      exit(1);
    } else {
      cout << "Shared memory found" << endl;
    }

    if (wm_base == nullptr) {
      cerr << "XDG WM Base not found" << endl;
      exit(1);
    } else {
      cout << "XDG WM Base found" << endl;
    }

    if (output == nullptr) {
      cerr << "WL Output is not found" << endl;
      exit(1);
    } else {
      cout << "WL Output is found" << endl;
    }
  }

  wl_buffer *draw_frame() {
    int width = 256, height = 256;
    int stride = width * 4;
    int shm_pool_size = height * stride * 2;

    int fd = ShmUtil::allocate_shm_file(shm_pool_size);

    if (fd < 0) {
      cerr << "Could not allocate shared memory" << endl;
      exit(1);
    }

    u8 *pool_data = static_cast<u8 *>(mmap(
        nullptr, shm_pool_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));

    if (pool_data == MAP_FAILED) {
      close(fd);
      cerr << "Mem Map failed" << endl;
      exit(1);
    }

    pool = wl_shm_create_pool(shm, fd, shm_pool_size);

    int index = 0;
    int offset = height * stride * index;

    wl_buffer *buffer = wl_shm_pool_create_buffer(
        pool, offset, width, height, stride, WL_SHM_FORMAT_XRGB8888);

    wl_shm_pool_destroy(pool);
    close(fd);

    u32 *pixels = (u32 *)&pool_data[offset];
    for (int y = 0; y < height; ++y) {
      for (int x = 0; x < width; ++x) {
        if ((x + y / 8 * 8) % 16 < 8) {
          pixels[y * width + x] = 0xFF666666;
        } else {
          pixels[y * width + x] = 0xFFEEEEEE;
        }
      }
    }

    munmap(pool_data, shm_pool_size);
    wl_buffer_add_listener(buffer, &buffer_listener, nullptr);
    return buffer;
  }

  static void registry_global(void *data, struct wl_registry *registry, u32 id,
                              const char *interface, u32 version) {
    Client *state = static_cast<Client *>(data);
    string reg_name = interface;

    cout << "found: " << reg_name << endl;

    std::ofstream file("capabilities.txt", std::ios::app);
    file << reg_name << " v" << version << endl;

    if (reg_name == wl_shm_interface.name) {
      state->shm = static_cast<wl_shm *>(
          wl_registry_bind(registry, id, &wl_shm_interface, version));
    } else if (reg_name == wl_compositor_interface.name) {
      state->compositor = static_cast<wl_compositor *>(
          wl_registry_bind(registry, id, &wl_compositor_interface, version));
    } else if (reg_name == xdg_wm_base_interface.name) {
      state->wm_base = static_cast<xdg_wm_base *>(
          wl_registry_bind(registry, id, &xdg_wm_base_interface, version));
      xdg_wm_base_add_listener(state->wm_base, &wm_base_listener, state);
    }
  }

  // does nothing, as is apparent
  static void registry_remove(void *data, struct wl_registry *reg, u32 id) {
    cout << "nuh uh" << endl;
  }

  static void wm_base_ping(void *data, struct xdg_wm_base *wm_base,
                           u32 serial) {
    xdg_wm_base_pong(wm_base, serial);
  }

  static void buffer_release(void *data, wl_buffer *buffer) {
    wl_buffer_destroy(buffer);
  }

  static void surface_configure(void *data, xdg_surface *wm_surface,
                                u32 serial) {
    Client *state = static_cast<Client *>(data);
    xdg_surface_ack_configure(wm_surface, serial);

    wl_buffer *buffer = state->draw_frame();
    wl_surface_attach(state->surface, buffer, 0, 0);
    wl_surface_commit(state->surface);
  }

  static void geometry_listener(void *data, wl_output *output, i32 x, i32 y,
                                i32 width, i32 height, i32 subpixel,
                                const char *make, const char *model,
                                i32 transform) {
    cout << "x: " << x << " y: " << y << endl;
  }
};

const wl_registry_listener Client::registry_listener = {
    .global = registry_global,
    .global_remove = registry_remove,
};

const xdg_wm_base_listener Client::wm_base_listener = {.ping = wm_base_ping};

const wl_buffer_listener Client::buffer_listener = {.release = buffer_release};

const xdg_surface_listener Client::surface_listener = {.configure =
                                                           surface_configure};

int main(int argc, char *argv[]) {
  Client app;
  return 0;
}
