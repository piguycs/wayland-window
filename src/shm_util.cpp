#include "shm_util.h"

#include <cerrno>
#include <cstdlib>
#include <ctime>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>

void randname(char *buf) {
  std::timespec ts;
  clock_gettime(CLOCK_REALTIME, &ts);

  long r = ts.tv_nsec;

  for (int i = 0; i < 6; ++i) {
    buf[i] = 'A' + (r & 15) + (r & 16) * 2;
    r >>= 5;
  }
}

int create_shm_file(void) {
  int retries = 100;

  do {
    char name[] = "/wl_shm-XXXXXX";
    randname(name + sizeof(name) - 7);
    --retries;

    int fd = shm_open(name, O_RDWR | O_CREAT | O_EXCL, 0600);

    if (fd >= 0) {
      shm_unlink(name);
      return fd;
    }
  } while (retries > 0 && errno == EEXIST);
  return -1;
}

int allocate_shm_file_int(size_t size) {
  int fd = create_shm_file();

  if (fd < 0) {
    return -1;
  }

  int ret;

  do {
    ret = ftruncate(fd, size);
  } while (ret < 0 && errno == EINTR);

  if (ret < 0) {
    close(fd);
    return -1;
  }

  return fd;
}

int ShmUtil::allocate_shm_file(std::size_t size) {
  return allocate_shm_file_int(size);
}
