#ifndef SHM_UTIL_H
#define SHM_UTIL_H

#include <cstddef>

class ShmUtil {
public:
    static int allocate_shm_file(std::size_t size);
};

#endif // SHM_UTIL_H
